package com.bel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author beliveli on 2017/7/11.
 */
@SpringBootApplication
//@Import({DynamicDataSourceRegister.class})
//@MapperScan("com.beliveli.dao") // 也可以在dao层接口上标注@Mapper注解
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
